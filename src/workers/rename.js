const fs = require('fs')
const { Explorer } = require("./explorer");

const { SRC, EXAMPLE, NOW_UI, PACKAGE, ROOT_INDEX } = require('../constants/paths');
const { Survey } = require('../survey/survey');
const { NowWorker } = require('./worker');
const { xLog } = require('../log/log');
exports.Rename = class extends NowWorker {
  constructor(options) {
    super(options)
    this.options = options;
  }
  async run() {
    this.__getNewDir()
    const { options: { oldName, newName, directory, newDirectory } } = this;
    if (oldName == newName) {
      xLog.fail('Component name wasn\'t changed.');
      return
    }

    const filesToChangeContent = [
      `${EXAMPLE}${oldName}.js`,
      `${directory}/${oldName}.js`,
      `${directory}/index.js`,
      ROOT_INDEX,
      PACKAGE,
      NOW_UI
    ]
    const directivesToRename = [
      { old: `${EXAMPLE}${oldName}.js`, new: `${EXAMPLE}${newName}.js` },
      { old: `${directory}/${oldName}.js`, new: `${directory}/${newName}.js` },
      { old: directory, new: newDirectory }
    ]
    this.changeContentInFiles(filesToChangeContent,oldName, newName);
    this.renameFiles(directivesToRename)

    const explorer = new Explorer();
    const filesLeftToChangeRefs = explorer.searchForTextInFiles(this.__getRegex(oldName))

    if (filesLeftToChangeRefs.length > 0) {
      const doRenameFiles = await this.__askForRenamingRemaningFiles(filesLeftToChangeRefs)
      if (doRenameFiles) 
        this.changeContentInFiles(filesLeftToChangeRefs, oldName, newName)
    }
  }
  getRenameOptions() {
    const { options: { newName, newDirectory } } = this
    return { componentName: newName, directory: newDirectory }
  }


  __getComponent() {
    const explorer = new Explorer();
    return explorer.searchComponent(oldName);
  }

  __changeContent(path) {
    const { options: { oldName, newName } } = this;

    if (!fs.existsSync(path)) {
      return
    }

    let fileContent = fs.readFileSync(path, 'utf8')


    fileContent = this.__replaceContent(fileContent, oldName, newName)

    this.__createFile(path, fileContent);
    xLog.success(`Content added to ${path}`)
  }

  __getNewDir() {
    const { options: { directory, oldName, newName } } = this
    this.options.newDirectory = directory.replace(oldName, newName)
  }
  __replaceContent(content, oldName, newName) {

    const rx = this.__getRegex(oldName)
    return content.replace(rx, newName)
  }
  changeContentInFiles(files, oldName, newName) {
    for (const file of files) {
      if (fs.existsSync(file)) {
        let fileContent = fs.readFileSync(file, 'utf8')
        fileContent = this.__replaceContent(fileContent, oldName, newName)
        this.__createFile(file, fileContent)
      }
    }
  }
  renameFiles(files) {
    for (const file of files) {
      this.__rename(file.old, file.new)
    }
  }
  __getRegex(name) {
    return new RegExp(`(?<=<|\/|'|"|:)(${name})(?![a-z|-])`, 'g')
  }
  async __askForRenamingRemaningFiles(files) {
    const { options: { oldName, newName } } = this;
    const filesLeftToChangeRefs = {
      type: 'confirm',
      name: 'filesLeftToChangeRefs',
      message: `There are some files contains references to ${oldName}:${JSON.stringify(files)}\nDo you want to change references to ${newName}`,
      default: false,
      next: false,
      nextNegative: false
    }

    const survey = new Survey({}, { filesLeftToChangeRefs }, "filesLeftToChangeRefs")
    const ans = await survey.askAll();
    return ans.filesLeftToChangeRefs
  }
}

/*
Objects to change ::
  [] .src/../component/
  [] /../component/component.js
  [] /../component/index.js - change import
  [] ./src/index.js
  [] /examples/component.js
  [] ./now-ui.json
  [] ./package.json
  [] neighbour files
*/