const fs = require('fs')
const path = require('path')
const slash = require('slash')
const { SRC } = require('../constants/paths')
const { NowWorker } = require('./worker')
exports.Explorer = class extends NowWorker{
  constructor(options) {
    super(options)
    this.components = []
  }
  isComponentExists() {

  }

  getComponents() {
    return this.__searchComponents()
  }

  searchComponent(componentName) {
    return this.__searchComponents().filter(component => component.name == componentName)
  }

  __searchComponents() {
    this.components = []
    if (!fs.existsSync(SRC)) return []
    this.__collectComponentsRecursively({ path: SRC, name: 'src' })
    return this.components;
  }

  __collectComponentsRecursively(src) {
    if (this.__isDirIsComponent(src.name, this.__getFiles(src))) {
      this.components.push(src)
    }
    const dirs = this.__getDirectories(src)

    dirs.forEach(dir => {
      this.__collectComponentsRecursively(dir)
    })
  }

  __getDirectories(source) {
    const isDirectory = source => fs.lstatSync(source.path).isDirectory()
    return fs.readdirSync(source.path).map(name => {
      return {
        path: slash(path.join(source.path, name)),
        name
      }
    }).filter(isDirectory)
  }

  __getFiles(source) {
    const isFile = source => fs.lstatSync(source.path).isFile()
    return fs.readdirSync(source.path).map(name => {
      return {
        path: slash(path.join(source.path, name)),
        fullName: name,
        name: name.split('.').slice(0, -1).join('.')
      }
    }).filter(isFile)
  }

  __isDirIsComponent(dirName, files) {
    return files.some(file => file.name === dirName)
  }
  searchForTextInFiles(rx){
    return this.__searchInFilesRecursively({path: SRC, name: 'src'}, [], rx).map(x=>x.path)
  }
  __searchInFilesRecursively(src, files, rx) {
    
    files.push(...this.__getFiles(src).filter(x=>this.__doHaveMatch(x.path,rx)))
    
    const dirs = this.__getDirectories(src)

    dirs.forEach(dir => {
      this.__searchInFilesRecursively(dir, files, rx)
    })
    return files
  }
  __doHaveMatch(path, rx){
    const fileContent = fs.readFileSync(path,'utf8');
    return fileContent.match(rx)
  }
}