const fs = require('fs')
const paths = require('../constants/paths')
const { Exec } = require('./exec')
const { indexFile, component, actions, view, styles } = require('../templates/create')
const { NowWorker } = require('./worker');
const {xLog} = require('../log/log')
exports.Builder = class extends NowWorker{
  constructor(options) {
    super(options)

    const { directory} = this.options

    this.options.rootFolder = directory ? directory : paths.SRC;
    this.__addPrefix()
    this.initComponentFolderName()
  }

  run() {
    const { options: { executable } } = this

    if (this.isComponentExists()) {
      xLog.warn(`Looks like component with such name is already exists! Try another name`)
      return
    }
    
    this.checkImportantDirectives();
    this.checkImportantFiles()
    this.createComponentFiles();
    this.importToRootIndex()
  }
  isComponentExists() {
    return this.__checkRoot() || this.__checkExample()
  }

  createComponentFiles() {
    const { options: { componentName, componentFolder } } = this
    this.__createFile(`${componentFolder}/${componentName}.js`, component(componentName))
    this.__createFile(`${componentFolder}/index.js`, indexFile(componentName))
    this.__createFile(`${componentFolder}/actions.js`, actions(componentName))
    this.__createFile(`${componentFolder}/view.js`, view(componentName))
    this.__createFile(`${componentFolder}/styles.scss`, styles(componentName))
    this.__importComponents(`${componentFolder}/index.js`)
  }

  importToRootIndex() {
    const { componentName } = this.options;
    const { ROOT_INDEX } = paths;
    const template = `\nimport './${componentName}';`
    fs.appendFileSync(ROOT_INDEX, template);
  }

  checkImportantDirectives() {

    const { options: { componentFolder } } = this

    if (!fs.existsSync(componentFolder))
      this.__mkdir(componentFolder)

  }

  checkImportantFiles() {
    const { ROOT_INDEX } = paths

    if (!fs.existsSync(ROOT_INDEX))
      this.__createFile(ROOT_INDEX, '')
  }

  initComponentFolderName() {
    const { options: { rootFolder, componentName } } = this
    this.options.componentFolder = `${rootFolder}${componentName}`
  }
  __addPrefix(){
    const { options: { vendorPrefix, componentName } } = this
    
    if(!vendorPrefix)
      return 
    
    const rx = new RegExp(`^${vendorPrefix}`)
    
    if(!rx.test(componentName))
      this.options.componentName = `${vendorPrefix}-${componentName}`
  }
  __checkRoot() {
    const { componentFolder } = this.options;
    return fs.existsSync(componentFolder)
  }

  __checkExample() {
    const { componentName } = this.options;
    const { EXAMPLE } = paths
    return fs.existsSync(`${EXAMPLE}${componentName}.js`)
  }

  __importComponents(pathToIndexFile) {
    let imports = '\n//OOB components listed below:\n'
    const { innerComponents } = this.options;
    imports = innerComponents.reduce(
      (acc, current) => {
        return acc + `import '@servicenow/${current}';\n`
      },
      imports)
    fs.appendFileSync(pathToIndexFile, imports);
    xLog.success('Inner components are imported!')
  }

}