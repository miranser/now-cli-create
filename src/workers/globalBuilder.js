const { Registrar } = require('./registrar');
const { Builder } = require('./builder');
const { xLog } = require('../log/log');

exports.GlobalBuilder = class extends Builder {
  constructor(options) {
    super(options);
    
  }
  run() {
    super.run()
  }

  isVendorPrefixProvided() {
    const { options: { vendorPrefix } } = this
    return !!vendorPrefix
  }
}

/*
[*] Check if component exists
  [*] Check if ./src/ exists
  [*] Check if relative path exists
  [*] Check if relative path to component exists
  [*] Check if ./example/ exists
  [*] Check if example file exists
[] Create component from templates
  [*] component_name.js
  [*] index.js
    [*] import inner-components
  [*] actions.js
  [*] view.js
  [*] styles.scss
[] Register component
  [*] create file in ./example/
  [*] add import to root index.js
  [*] add run script to package.json
  -g[*] add component to now-ui.json
*/