const fs = require('fs');
const { xLog } = require('../log/log');

exports.NowWorker = class {
  constructor(options){
    this.options = options;
  }
  run(){

  }
  __createFile(path, content) {
    fs.writeFileSync(path, content)
    xLog.success(`File ${path} Changed!`)
  }
  __mkdir(path) {
    fs.mkdirSync(path, { recursive: true })
    xLog.warn(`Directory ${path} not found. Created`)
  }
  __rename(oldPath, newPath) {
    xLog.info(`rename' ${oldPath} --> ${newPath}`)
    if(fs.existsSync(oldPath))
      fs.renameSync(oldPath, newPath)
  }
}