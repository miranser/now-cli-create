const fs = require('fs')
const { NOW_UI } = require('../constants/paths')
const { nowUi } = require('../templates/system-files')
const { Exec } = require('./exec')
const { Explorer } = require('./explorer')
const { Rename } = require('./rename')
const { NowWorker } = require('./worker')
exports.Registrar = class extends NowWorker{
  constructor(options) {
    super(options)
    const { options: {label, description, icon, innerComponents} } = this
    this.__addPrefix()
    this.name = this.options.componentName;
    this.__getUiBuilder()
    this.uiBuilder.label = label || '';
    this.uiBuilder.description = description || '';
    this.innerComponents = innerComponents || [];
    this.uiBuilder.icon = icon || '';
  }
  run() {
    this.__checkNowUi();
    this.__register();
  }
  __checkNowUi() {
    if (!fs.existsSync(NOW_UI))
      this.__createFile(NOW_UI, nowUi)
  }
  __register() {
    const { name, uiBuilder, innerComponents } = this;

    let rawdata = fs.readFileSync(NOW_UI);
    let nowUiContent = JSON.parse(rawdata);

    if (!nowUiContent.components)
      nowUiContent = { components: {} };

    nowUiContent.components[name] = { uiBuilder, innerComponents }
    rawdata = JSON.stringify(nowUiContent, null, 2);
    this.__createFile(NOW_UI, rawdata)
  }
  __getUiBuilder() {
    this.uiBuilder = {
      associatedTypes: [
        'global.core',
        'global.landing-page'
      ],
      category: 'primitives'
    }
  }
  __addPrefix(){
    const { options: { vendorPrefix, componentName } } = this
    
    if(!vendorPrefix)
      return 
    
    const rx = new RegExp(`^${vendorPrefix}`)
    
    if(!rx.test(componentName)){
      const oldName = this.options.componentName
      const newName = `${vendorPrefix}-${componentName}`
      this.options.oldName = oldName
      this.options.componentName = newName
      this.options.newName = newName
    }
  }
  static getNotRegistered() {
    const explorer = new Explorer();

    const allComponents = explorer.getComponents()
    const registeredComponents = this.getRegistered()

    return allComponents
      .filter(x => !registeredComponents.includes(x.name))
  }

  static isRegistered(componentName) {
    return this.getRegistered().includes(componentName)
  }

  static getRegistered() {
    if (!fs.existsSync(NOW_UI)) return [];

    let rawdata = fs.readFileSync(NOW_UI);
    let nowUiContent = JSON.parse(rawdata);

    const { components } = nowUiContent

    return Object.keys(components)
  }
}