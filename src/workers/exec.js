const { EXAMPLE, PACKAGE } = require('../constants/paths')
const { example, packageScript } = require('../templates/create')
const { packageJson } = require('../templates/system-files')
const fs = require('fs');
const path = require('path')
const slash = require('slash')
const { Explorer } = require('./explorer');
const { NowWorker } = require('./worker')
const { componentName, directory } = require('../questions/create');
const { xLog } = require('../log/log');
exports.Exec = class extends NowWorker{
  constructor(options) {
    super(options)
  }
  run() {
    const {options: {componentName}} = this;
    if(this.__checkIfExists()){
      xLog.fail(`${EXAMPLE}${componentName}.js is already exists. Aborting.` )
      return
    }
    this.__checkExampleFolder();
    this.__checkPackageJson();
    this.__createRunScript();
    this.__createExample();
    xLog.success('Component is registered as executable')
  }

  __checkIfExists(){
    const {options: {componentName}} = this;
    fs.existsSync(`${EXAMPLE}${componentName}.js`)
  }
  __checkExampleFolder() {
    if (!fs.existsSync(EXAMPLE))
      this.__mkdir(EXAMPLE)
  }

  __checkPackageJson() {
    if (!fs.existsSync(PACKAGE))
      this.__createFile(PACKAGE, packageJson)
  }

  __createExample() {
    const { options: {componentFolder, componentName} } = this;
    this.__createFile(`${EXAMPLE}${componentName}.js`, example(componentFolder, componentName))
  }

  __createRunScript() {
    const { componentName } = this.options;

    let rawData = fs.readFileSync(PACKAGE);
    let packageContent = JSON.parse(rawData);

    const template = packageScript(componentName)

    packageContent.scripts[template.name] = template.value;
    rawData = JSON.stringify(packageContent, null, 2);
    this.__createFile(PACKAGE, rawData)
    xLog.success('Run script added to package.json')
  }


  static getNotExecutable() {
    const explorer = new Explorer();

    const allComponents = explorer.getComponents()
    const executableComponents = this.getExecutables()

    return allComponents
      .filter(x => !executableComponents.includes(x.name))
  }

  static isExecutable(componentName) {
    return this.getExecutables().includes(componentName)
  }

  static getExecutables() {
    if (!fs.existsSync(EXAMPLE)) return []

    const isFile = source => fs.lstatSync(source.path).isFile()

    return fs.readdirSync(EXAMPLE).map(name => {
      return {
        path: slash(path.join(EXAMPLE, name)),
        fullName: name,
        name: name.split('.').slice(0, -1).join('.')
      }
    }).filter(isFile)
  }
}