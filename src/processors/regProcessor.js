const arg = require('arg')

const { Survey } = require("../survey/survey");
const { Processor } = require("./processor");
let QUESTIONS = require('../questions/create');
const { Explorer } = require('../workers/explorer');
const { Registrar } = require('../workers/registrar');
const { selectNotRegistered } = require('../questions/special-questions');
const { Rename } = require('../workers/rename');
exports.RegProcessor = class extends Processor {
  constructor() {
    super()
  }
  async process() {
    super.process()
  }
  buildWorker() {
   this.worker = new Registrar(this.options);
   this.workers.push(new Rename(this.worker.options))
  }

  buildSurveyQuestions() {
    const { options } = this
    let firstQuestion = 'vendorPrefix'
    if(!options.name){
      QUESTIONS.componentName = selectNotRegistered
      QUESTIONS.innerComponents.next = false
      firstQuestion = 'componentName'
    }
    this.questions = QUESTIONS
    this.setFirstQuestion(firstQuestion)
  }
  parseCliArgsIntoOptions() {
    const args = arg(
      {
        '--name': String,
        '--vendor-prefix': String,
        '--label': String,
        '--description': String,
        '--inner-components': [String],
        '--icon': String,
        '-n': '--name',
        '-v': '--vendor-prefix',
        '-i': '--inner-components',
        '-l': '--label',
        '-d': '--description',
        '-o': '--icon',
      },
      {
        argv: process.argv.slice(2),
      }
    );
    return {
      componentName: args['--name'] || false,
      vendorPrefix: args['--vendor-prefix'] || false,
      label: args['--label'] || false,
      description: args['--description'] || false,
      innerComponents: args['--inner-components'] || false,
      icon: args['--icon'] || false
    };
  }

}