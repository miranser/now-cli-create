const { Processor } = require("./processor");
const arg = require('arg')
const icons = require('../constants/icons')
const fuzzy = require('fuzzy')
let QUESTIONS = require('../questions/create');
const { Survey } = require("../survey/survey");
const { selectNotExecutable } = require('../questions/special-questions');
const { Exec } = require("../workers/exec");
exports.ExecProcessor = class extends Processor {
  constructor(parseCliArgs) {
    super(parseCliArgs)
    this.options = {}
  }
  async process() {
    super.process()
  }
  buildWorker() {
    this.worker = new Exec(this.options)

  }

  buildSurveyQuestions() {
    this.questions = { selectNotExecutable }
    this.setFirstQuestion('selectNotExecutable')
  }
  
}
