const { Processor } = require("./processor");
const arg = require('arg')
const QUESTIONS = require('../questions/create')
const { Survey } = require('../survey/survey')
const { Builder } = require('../workers/builder')
const { GlobalBuilder } = require('../workers/globalBuilder');
const { Exec } = require("../workers/exec");
const { Registrar } = require("../workers/registrar");
exports.CreateProcessor = class extends Processor {
  constructor(parseCliArgs) {
    super(parseCliArgs)
  }
  async process() {
    super.process()
  }

  buildWorker() {
    const {options, options:{global, executable}} = this
    
    this.worker = !global ?
      new Builder(options) :
      new GlobalBuilder(options);
      
      if(executable){
        this.workers.push(new Exec(this.worker.options))
      }
      if(global){
        this.workers.push(new Registrar(this.worker.options))
      }
  }

  buildSurveyQuestions = () => {
    this.questions = QUESTIONS;
    this.setFirstQuestion('componentName')
  }
  parseCliArgsIntoOptions() {
    let args = {}
    if (this.parseCliArgs) {
      args = arg(
        {
          '--component': String,
          '--directory': String,
          '--global': Boolean,
          '--vendor-prefix': String,
          '--label': String,
          '--description': String,
          '--inner-components': [String],
          '--icon': String,
          '--force': Boolean,
          '--executable': Boolean,
          '-c': '--component',
          '-g': '--global',
          '-v': '--vendor-prefix',
          '-i': '--inner-components',
          '-l': '--label',
          '-d': '--description',
          '-o': '--icon',
          '-d': '--directory',
          '-f': '--force',
          '-e': '--executable'
        },
        {
          argv: process.argv.slice(2),
        }
      );
    }
    return{
      componentName: args['--component'] || false,
      global: args['--global'] || false,
      vendorPrefix: args['--vendor-prefix'] || false,
      label: args['--label'] || false,
      description: args['--description'] || false,
      innerComponents: args['--inner-components'] || false,
      icon: args['--icon'] || false,
      directory: args['--directory'] || false,
      force: args['--force'] || false,
      executable: args['--executable'] || false
    };
  }
}