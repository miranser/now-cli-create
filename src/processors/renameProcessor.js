const { Survey } = require("../survey/survey");
const { Rename } = require("../workers/rename");
const { Processor } = require("./processor");
let {selectComponent} = require('../questions/special-questions')
exports.RenameProcessor = class extends Processor{
  constructor(parseCliArgs){
    super(parseCliArgs)
  }
  async process(){
    super.process()
  }
  buildSurveyQuestions(){
    const newName = {
      type: 'text',
      name: 'newName',
      message: '\nEnter component new name\n',
      default: 'my-component',
      next: false
    }
    selectComponent.extracts = [
      {option: 'oldName', value:'name'}, 
      {option: 'directory', value: 'path'}
    ]
    selectComponent.name = 'oldName'
    selectComponent.next = 'newName'
    this.questions = {selectComponent, newName}
    this.setFirstQuestion('selectComponent');
  }
  buildWorker(){
    this.worker = new Rename(this.options)
  }
  
  
}