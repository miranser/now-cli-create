const { xLog } = require("../log/log")
const { Survey } = require("../survey/survey")

exports.Processor = class {
  constructor(parseCliArgs) {
    this.parseCliArgs = parseCliArgs
    this.options = {}
    this.workers = []
  }
  async process() {
    const { parseCliArgs } = this


    if (parseCliArgs)
      this.options = this.parseCliArgsIntoOptions()

    this.buildSurveyQuestions()
    this.buildSurvey()

    if (!this.options.force)
      this.options = await this.survey.askAll()

    this.buildWorker()

    this.worker.run()
    if (this.workers.length > 0) {
     
      for (const wrk of this.workers) {
        wrk.run()
      }
    }
    xLog.success('Process complete')
  }
  buildWorker() {

  }

  buildSurveyQuestions() {

  }
  setFirstQuestion(firstQuestionName) {
    this.firstQuestionName = firstQuestionName
  }
  buildSurvey() {
    const { options, questions, firstQuestionName } = this
    this.survey = new Survey(options, questions, firstQuestionName)
  }

  parseCliArgsIntoOptions() {
    return {}
  }
}