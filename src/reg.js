#! /usr/bin/env node

const { RegProcessor } = require("./processors/regProcessor")


register()


function register(){
  const processor = new RegProcessor(true)
  processor.process()
}

