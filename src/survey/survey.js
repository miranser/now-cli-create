let inquirer = require('inquirer')
inquirer.registerPrompt('autocomplete', require('inquirer-autocomplete-prompt'))
exports.Survey = class {
  constructor(options, questionSet, startQuestionName) {
    this.options = options;
    this.questionSet = questionSet
    this.startQuestion = questionSet[startQuestionName]
    this.answers = {}
  }
  async askAll() {
    const { questionSet, startQuestion, options } = this
    let { answers } = this
    let currentQuestion = startQuestion
    while (true) {
      const { name, extracts } = currentQuestion
      if (!options[name]) {
        const answer = await inquirer.prompt([currentQuestion])
        this.__extract(extracts, answer[name], name)
        
        if(!currentQuestion.next)break

        currentQuestion = answer[name] !== false ?
          questionSet[currentQuestion.next] :
          questionSet[currentQuestion.nextNegative];
        continue;
      }
      currentQuestion = questionSet[currentQuestion.next]
    }
    return answers
  }
  __extract(extractions, answer, name){
    let {answers} = this

    if(!extractions){
      answers[name] = answer
      return
    }
    
    for(const ex of extractions){
      const option = ex.option
      const value = answer[ex.value]
      answers[option] = value
    }
  }
}