exports.indexFile = (name) => {
  return `import './${name}'`
}
exports.component = (name) => {
  return `
import { createCustomElement } from '@servicenow/ui-core';
import snabbdom from '@servicenow/ui-renderer-snabbdom';
import styles from './styles.scss';
import view from './view'
import actions from './actions'
createCustomElement('${name}', {
	renderer: { type: snabbdom },
    view,
    styles,
	...actions,
	initialState: {},
	properties: {}
});
`
}
exports.actions = (name) => {
  return `
import { actionTypes } from '@servicenow/ui-core';
import { createHttpEffect } from '@servicenow/ui-effect-http';
const { COMPONENT_BOOTSTRAPPED} = actionTypes;

export default {
  actionHandlers: {
    [COMPONENT_BOOTSTRAPPED]: (coeffects) => { console.log("COMPONENT_BOOTSTRAPPED: ${name}") },
  }
}
`
}
exports.view = (name) => {
  return `
export default (state) => {
    return (
    <h1>${name}</h1>
    );
};
    `
}
exports.example = (path, name) => {
  return `
import '.${path}';

const el = document.createElement('DIV');
document.body.appendChild(el);

el.innerHTML = \`		
<${name}></${name}>
\`;
  `
}
exports.packageScript = (name) => {
  return {
    name: `start:${name}`,
    value: `now-cli develop --open --entry=./${name}.js`
  };
}
exports.styles = () => {
  return '@import "@servicenow/sass-kit/host";'
}
