#! /usr/bin/env node
const { ExecProcessor } = require('./processors/execProcessor');

run();

async function run() {
  const proc = new ExecProcessor(true);
  proc.process()
}
