#! /usr/bin/env node

const { Survey } = require("./survey/survey");
const QUESTIONS = require("./questions/welcome");
const { CreateProcessor } = require("./processors/createProcessor");
const { ExecProcessor } = require("./processors/execProcessor");
const { RegProcessor } = require("./processors/regProcessor");
const { xLog } = require("./log/log");
const { RenameProcessor } = require("./processors/renameProcessor");

run();

async function run() {

  const survey = new Survey({ action: false }, QUESTIONS, 'action')
  const options = await survey.askAll()
  let processor;
  switch (options.action) {
    case 'create': processor = new CreateProcessor(false); break;
    case 'exec': processor = new ExecProcessor(false); break;
    case 'reg': processor = new RegProcessor(false); break;
    case 'ren': processor = new RenameProcessor(false); break;
    default: break
  }
  
  processor.process()
  //xLog.info('End of process')
}






import './holy-molly';