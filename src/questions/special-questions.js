const fuzzy = require('fuzzy');
const icons = require('../constants/icons');
const compoents = require('../constants/components');
const { Exec } = require('../workers/exec');
const { Registrar } = require('../workers/registrar');
const { vendorPrefix } = require('./create');
const { Explorer } = require('../workers/explorer');
const notExecutable = Exec.getNotExecutable().map(x=>{return {name: x.name, value: x}})
const notRegistered = Registrar.getNotRegistered().map(x=>{return {name: x.name, value: x}})
const allComponents = new Explorer().getComponents()
exports.selectNotRegistered = {
  type: 'autocomplete',
  name: 'componentName',
  message: '\nI found some not registered components. Please, select one\n',
  default: '',
  source: searchNotRegistered,
  pageSize: 10,
  next:'vendorPrefix',
  extracts:[{option: 'componentName', value:'name'}, {option: 'directory', value: 'path'}]
}
exports.selectNotExecutable = {
  type: 'autocomplete',
  name: 'componentName',
  message: '\nI found some not executable components. please, select one\n',
  default: '',
  source: searchNotExecutable,
  pageSize: 10,
  next:false,
  extracts:[{option: 'componentName', value:'name'}, {option: 'componentFolder', value: 'path'}]
}
exports.selectIcon = {
  type: 'autocomplete',
  name: 'icon',
  message: '\nSelect icon\n',
  default: '',
  source: searchIcons,
  pageSize: 10,
  next: 'innerComponents'
}
exports.selectInnerComponents = {
  type: 'list',
  name: 'componentName',
  message: '\nSelect components\n',
  default: '',
  source: searchInnerComponents,
  pageSize: 10
}
exports.selectComponent = {
  type: 'autocomplete',
  name: 'componentName',
  message: '\nSelect component\n',
  default: '',
  source: searchNotExecutable,
  pageSize: 10,
  next:false,
  extracts:[{option: 'componentName', value:'name'}, {option: 'directory', value: 'path'}]
}

const options = {
  pre: '<'
, post: '>'
, extract: function(el) { return el.name; }
};

function searchIcons(answers, input) {
  input = input || '';
  return new Promise(function (resolve) {
    const fuzzyResult = fuzzy.filter(input, icons)
    resolve(
      fuzzyResult.map(function (el) {
        return el.original;
      })
    );
  });
}
function searchNotExecutable(answers, input) {
  input = input || '';
  return new Promise(function (resolve) {
    const fuzzyResult = fuzzy.filter(input, notExecutable, options)
    resolve(
      fuzzyResult.map(function (el) {
        return el.original;
      })
    );
  });
}
function searchNotRegistered(answers, input) {
  input = input || '';
  return new Promise(function (resolve) {
    const fuzzyResult = fuzzy.filter(input, notRegistered, options)
    resolve(
      fuzzyResult.map(function (el) {
        return el.original;
      })
    );
  });
}
function searchInnerComponents(answers, input) {
  input = input || '';
  return new Promise(function (resolve) {
    const fuzzyResult = fuzzy.filter(input, compoents)
    resolve(
      fuzzyResult.map(function (el) {
        return el.original;
      })
    );
  });
}
function searchInnerComponents(answers, input) {
  input = input || '';
  return new Promise(function (resolve) {
    const fuzzyResult = fuzzy.filter(input, allComponents, options)
    resolve(
      fuzzyResult.map(function (el) {
        return el.original;
      })
    );
  });
}