const components = require('../constants/components')
module.exports = {
  action: {
    type: 'list',
    name: 'action',
    message: 'What do you want to perform?',
    choices: [
      {name:'Create component', value: 'create'},
      {name: 'Register component', value: 'reg'},
      {name: 'Make component executable', value: 'exec'},
      {name: 'Rename', value: 'ren'}
    ],
    next: false
  },
}