const components = require('../constants/components')
const {SRC} = require('../constants/paths')
const {selectIcon} = require('./special-questions')
module.exports = {
  componentName:{
    type: 'text',
    name: 'componentName',
    message: '\nEnter component name\n',
    default: 'my-component',
    next: 'directory'
  },
  
  directory: {
    type: 'text',
    name: 'directory',
    message: 'Specify directory for your component\n',
    default: SRC,
    next: 'global'
  },
  global: {
    type: 'confirm',
    name: 'global',
    message: 'Do you want to register your component to be visible in UI Builder?\n',
    default: false,
    next: 'vendorPrefix',
    nextNegative: 'innerComponents'
  },
  vendorPrefix: {
    type: 'text',
    name: 'vendorPrefix',
    message: 'Please specify your vendor prefix\n',
    default: 'aaro2',
    next: 'label'
  },
  label: {
    type: 'text',
    name: 'label',
    message: 'Please enter your component label\n',
    default: 'My component',
    next: 'description'
  },
  description: {
    type: 'text',
    name: 'description',
    message: 'Describe your component\n',
    default: '',
    next: 'selectIcon'
  },
  selectIcon,
  innerComponents: {
    type: 'checkbox',
    name: 'innerComponents',
    message: 'Select components, you\'re planning to use\n',
    choices: components,
    next: 'executable'
  },
  executable: {
    type: 'confirm',
    name: 'executable',
    message: 'Should component run through npm script?\n',
    default: false,
    next: false
  }
}