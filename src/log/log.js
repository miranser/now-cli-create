const chalk = require('chalk');
const log = console.log

exports.xLog = class  {
  constructor(){}
  static info(text){
    log(chalk.cyan(text))
  }
  static warn(text){
    log(chalk.yellow(text))
  }
  static success(text){
    log(chalk.greenBright(text))
  }
  static fail(text){
    log(chalk.red(text))
  }
}