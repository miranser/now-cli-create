const envPath = ''
module.exports = {
  PACKAGE: `.${envPath}/package.json`,
  NOW_UI: `.${envPath}/now-ui.json`,
  SRC: `.${envPath}/src/`,
  EXAMPLE: `.${envPath}/example/`,
  ROOT_INDEX: `.${envPath}/src/index.js`,
}